import { Component, h, State, Prop } from '@stencil/core';

@Component({
  tag: 'tramites-consulta',
  styleUrl: 'tramites-consulta.css',
  shadow: true,
  assetsDirs: ['assets']
})
export class TramitesConsulta {

  @State() data: string;
  @State() dataTramites: Array<any>;
  @Prop() image = "icons8-atrás-52.png";

  async componentWillLoad() {

    let urltitulo = 'https://api-interno.www.gov.co/api/ficha-tramites-y-servicios/secciones/los-mas-consultados-en-home';
    let urlTramites = 'https://api-interno.www.gov.co/api/ficha-tramites-y-servicios/LoMasConsultado/ObtenerLoMasConsultado';

    await fetch(urltitulo).then(response => {
      response.json().then(json => {
        this.data = json.data;
        console.log(this.data);
      });
    });

    await fetch(urlTramites).then(response => {
      response.json().then(json => {
        this.dataTramites = json.data;
        console.log(this.dataTramites);
      });
    });

  }

  validateIcon(urlImagen: string) {

    let urlError = "http://img.freepik.com/free-vector/page-found-concept-illustration_114360-1869.jpg";

    if (urlImagen == "")
      urlImagen = urlError;

    return urlImagen;
  }


  render() {
    //const imageSrc = getAssetPath(`./assets/${this.image}`);

    return (
    
      <div class= "pt-5 general">
        <p class='tramites-consulta'>{this.data['titulo']}</p>
          <div class="row col-12" id="conteItemsCarrusel" >
            {this.dataTramites?.map(tramites =>
              <div class="col-ms-12 col-md-6 col-lg-4" >
                <a role="link" href="javascript:void(0)" >
                  <div class="container-img-span itemCarrusel" id="item">

                    <img src={this.validateIcon(tramites.iconoCategoria)}
                      alt="" />
                    <span>{tramites.nombre}</span>

                  </div>

                </a>

              </div>

            )}
            
        </div>
        <div class="flechaCarrusel">
            <a href="#items"  role="link" rel="noreferrer" target="">
                <span class="iconoAtras"></span>
            </a>
            <a href="#items" role="link" rel="noreferrer" target="">
                <span class="iconoAdelante"></span>
            </a>
        </div>
      </div>

    );
  }
}