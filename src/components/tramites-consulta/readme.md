# tramites-consulta



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default                 |
| -------- | --------- | ----------- | -------- | ----------------------- |
| `image`  | `image`   |             | `string` | `"icons8-atrás-52.png"` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
